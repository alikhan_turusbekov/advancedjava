import enttity.Group;
import enttity.Student;

import java.sql.*;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        System.out.println("PRINT ALL GROUPS");
        ArrayList<Group> groups = Group.getAllGroups();
        for (int i=0; i<groups.size(); i++){
            System.out.println(groups.get(i));
        }

        System.out.println();

        System.out.println("PRINT ALL STUDENTS");
        ArrayList<Student> students = Student.getAllStudents();
        for (int i=0; i<students.size(); i++){
            System.out.println(students.get(i));
        }

        System.out.println();

        System.out.println("PRINT ALL STUDENTS IN A GROUP WITH ID OF 101");
        ArrayList<Student> studentsByGroup = Student.getAllStudentsByGroupId(101);
        for (int i=0; i<studentsByGroup.size(); i++){
            System.out.println(studentsByGroup.get(i));
        }
    }
}
