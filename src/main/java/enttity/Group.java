package enttity;

import repository.GroupRep;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Group {
    private int groupId;
    private String groupName;
    private static GroupRep gr = new GroupRep();

    public Group(int groupId, String groupName){
        this.groupId = groupId;
        this.groupName = groupName;
    }

    public static ArrayList<Group> getAllGroups(){
        ArrayList<Group> groups = new ArrayList<Group>();
        ResultSet resultSet = gr.getAllGroups();
        try {
            while (resultSet.next()) {
                Group g = new Group(resultSet.getInt("groupId"),resultSet.getString("groupName"));
                groups.add(g);
            };
        } catch (Exception e) {
            e.printStackTrace();
        }
        return groups;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getGroupId() {
        return groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    @Override
    public String toString() {
        return "Group{" +
                "groupId=" + groupId +
                ", groupName='" + groupName + '\'' +
                '}';
    }
}
