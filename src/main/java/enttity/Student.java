package enttity;

import repository.GroupRep;
import repository.StudentRep;

import java.sql.ResultSet;
import java.util.ArrayList;

public class Student {
    private int studentId;
    private String studentName;
    private int phone;
    private int groupId;
    private static StudentRep sr = new StudentRep();

    public Student(int studentId, String studentName, int phone, int groupId){
        this.studentId = studentId;
        this.studentName = studentName;
        this.phone = phone;
        this.groupId = groupId;
    }

    public static ArrayList<Student> getAllStudentsByGroupId(int groupId){
        ArrayList<Student> students = Student.getAllStudents();
        ArrayList<Student> studentsByGroup = new ArrayList<Student>();
        for (int i=0; i<students.size(); i++){
            if (students.get(i).getGroup()==groupId){
                studentsByGroup.add(students.get(i));
            }
        }
        return studentsByGroup;
    }

    public static ArrayList<Student> getAllStudents(){
        ArrayList<Student> students = new ArrayList<Student>();
        ResultSet resultSet = sr.getAllStudents();
        try {
            while (resultSet.next()) {
                Student s = new Student(resultSet.getInt("studentId"),
                        resultSet.getString("studentName"),
                        resultSet.getInt("phone"),
                        resultSet.getInt("groupId"));
                students.add(s);
            };
        } catch (Exception e) {
            e.printStackTrace();
        }
        return students;
    }

    public void setGroup(int group) {
        this.groupId = group;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getPhone() {
        return phone;
    }

    public int getGroup() {
        return groupId;
    }

    public int getStudentId() {
        return studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", studentName='" + studentName + '\'' +
                ", phone=" + phone +
                ", group=" + groupId +
                '}';
    }
}
