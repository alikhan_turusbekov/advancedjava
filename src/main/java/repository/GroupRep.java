package repository;

import java.sql.*;

public class GroupRep {
    public ResultSet getAllGroups() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "alikhan");
            Statement statement = connection.createStatement();
            return statement.executeQuery("select * from educationalgroups");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
